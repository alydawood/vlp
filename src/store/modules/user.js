import axios from 'axios';
const state = {
    user: null,
};

const getters = {
    getUser: state => {
        return state.user;
    },

};

const actions = {

    setUser({commit},payload) {
        commit('setUser', payload);
    },
    fetchUser({commit}) {
        axios.defaults.headers.common['Authorization']=`Bearer ${localStorage.getItem('token')}`;
        axios.get('api/user').then((response)=>{
            commit('setUser',response.data.data)
       }).catch((err)=>{
            console.log(err);
       })

    }

};

const mutations = {
    setUser(state, payload) {

        state.user = payload ;


    }
};

export default {
    state, getters, actions, mutations,
}
