import Vue from 'vue'
import App from './App.vue'
import router from './router'
import Vuelidate from 'vuelidate'
import axios from 'axios';
import store from './store';
axios.defaults.baseURL="http://lara.lvp.cmsdeveloper.net";
axios.defaults.headers.common['Authorization']=`Bearer ${localStorage.getItem('token')}`;
Vue.config.productionTip = false
Vue.use(Vuelidate)
new Vue({
    router,
    store,
  render: h => h(App),
}).$mount('#app')
