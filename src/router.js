import Vue from 'vue';
import VueRouter from 'vue-router';
import Login from './components/Login'
import Register from './components/Register'
import Home from './components/Home'
import ForgotPassword from './components/ForgotPassword'
import ResetPassword from './components/ResetPassword'

Vue.use(VueRouter);

export default new VueRouter({
    mode: 'history',

    routes: [
        {
            path: '/login', name: 'login', component: Login,
            meta: { title: 'Login' }
        },
        {
            path: '/register', name: 'register', component: Register,
            meta: { title: 'Register' }
        },
        {
            path: '/home', name: 'home', component: Home,
            meta: { title: 'Home' }
        },
        {
            path: '/forgot', name: 'forgot', component: ForgotPassword,
            meta: { title: 'Forgot' }
        },
        {
            path: '/reset', name: 'reset', component: ResetPassword,
            meta: { title: 'Reset' }
        },
        {
            path: '/reset/:token',name: 'reset2', component: ResetPassword,
            meta: { title: 'Reset' }
        }
    ]
});
